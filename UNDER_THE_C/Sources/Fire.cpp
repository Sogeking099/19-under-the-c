#include "Headers/Fire.h"

Fire::Fire(int x, int y, QGraphicsItem *parent): StraightMotion(parent)
{
    setPixmap(QPixmap(":/images/attacks/fire-clipart-16.png"));
    setPos(x,y);
    setScale(0.2);

    setType(FIRE);
    setIsAlive(true);
}

