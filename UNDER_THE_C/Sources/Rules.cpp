#include "Headers/Rules.h"

Rules::Rules()
{
    QPointF buttonPos=mapToScene(200,100);
    setPos(buttonPos.x(), buttonPos.y());
    setZValue(10);

}

void Rules::setNumberOfLevel(int numberOfLevel)
{
    if(numberOfLevel==1)
        setPixmap(QPixmap(":/images/rules/HelpMimi.png"));
    else if(numberOfLevel==2)
        setPixmap(QPixmap(":/images/rules/HelpKoko.png"));
    else if(numberOfLevel==3)
        setPixmap(QPixmap(":/images/rules/HelpMermaid2.png"));
    else if(numberOfLevel==4)
        setPixmap(QPixmap(":/images/rules/HelpPinky.png"));
}
