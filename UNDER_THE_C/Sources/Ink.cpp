#include <Headers/Ink.h>
#include <QTimer>
#include <Headers/Game.h>
#include <QGraphicsScene>
#include <Headers/Enemy.h>

Ink::Ink(){

    setIsAlive(true);
    setType(INK);
    setPixmap(QPixmap(":/images/attacks/ink.png"));
    setScale(0.25);
    health_change=0;




}

void Ink::collision()
{
    auto colliding_items= collidingItems();
        foreach(auto item, colliding_items){
            if(typeid (*item)== typeid(PlayerMermaid) ){

            if(qgraphicsitem_cast<PlayerMermaid *>(item)->getShiled()==false){
                setHealthChange(1);
            }
            else{
                qgraphicsitem_cast<PlayerMermaid *>(item)->setShiled(false);
                setHealthChange(0);
            }

            setIsAlive(false);


            return;
        }

            else if(typeid (*item)== typeid(Bubble) ){

                qgraphicsitem_cast<Bubble *>(item)->setPixmap(QPixmap(":/images/attacks/darkbubble.png"));

                qgraphicsitem_cast<Bubble *>(item)->setScale(0.06);
                qgraphicsitem_cast<Bubble *>(item)->dark=true;





            }
        }

}
