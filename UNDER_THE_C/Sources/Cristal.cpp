#include "Headers/Cristal.h"

Cristal::Cristal(char type)
{
    setIsAlive(true);
    setType(CRISTAL);

    score_change=0;
    golden_collected = false;
    this->type = type;

    if(type == 'C'){
        setPixmap(QPixmap(":/images/collectables/cristal1.png"));
    }
    else if(type == 'K'){
        setPixmap(QPixmap(":/images/collectables/cristal2.png"));
    }
    else{
        setPixmap(QPixmap(":/images/collectables/potion.png"));
        setScale(0.1);
    }

    setMusicEmit(0);

}

void Cristal::collision()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (PlayerPlatform)){
            setMusicEmit(1);
            if(type == 'C')
                score_change=1;
            else if(type == 'K')
                golden_collected = true;
            setIsAlive(false);
        }
    }
}

char Cristal::getType()
{
    return type;
}

bool Cristal::getGoldenCollected()
{
    return golden_collected;
}
