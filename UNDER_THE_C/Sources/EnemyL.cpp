#include "Headers/EnemyL.h"
#include <QTimer>
#include <QDebug>
#include <QGraphicsScene>
#include <stdlib.h>
#include <QDebug>
#include "Headers/Health.h"
#include <Headers/PlayerPinky.h>
#include <Headers/StraightMotion.h>
#include <iostream>

//extern Game *game;

int EnemyL::brojac = 0;
EnemyL::EnemyL(int numberOfLife,StraightMotion *parent)/*:StraightMotion(parent)*/
{
    setType(ENEMYL);
    setIsAlive(true);

    setPixmap(QPixmap(":/images/enemies/spiderr.png"));
    setPos(2900, 1880);
    setScale(0.1);

    health = numberOfLife;
    health_change = 0;
    score_change = 0;
    dx = -10;
    dx1 = 10;
    direction=true;
    sizeOfPath=0;

    ID = brojac;
    brojac++;
}
int EnemyL::getNumberOfLife()const{
    return health;

}
void EnemyL::incNumberOfLife(){
    health--;
}

int EnemyL::getDX(){
    return dx;
}

void EnemyL::setDX(int value){
    dx = value;
}

int EnemyL::getDX1(){
    return dx1;
}

void EnemyL::setDX1(int value){
    dx1 = value;
}
