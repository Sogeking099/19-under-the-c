#include "Headers/Platform.h"

Platform::Platform()
{
    image_type = QPixmap(":/images/platforms/Brick_02.png");
    x=0;
    y=0;
    w=128;
    h=128;
}

Platform::Platform(char type)
{
    switch (type) {
    // tanak tip platforme
    //------------
    //---zxxxxc---
    //------------
    case 'z' :
        image_type = QPixmap(":/images/platforms/p1.png");
        x = 0;
        y = 0;
        w = 570/3;
        h = 250;
        break;
    case 'x' :
        image_type = QPixmap(":/images/platforms/p1.png");
        x = 570/3;
        y = 0;
        w = 570/3;
        h = 250;
        break;
    case 'c' :
        image_type = QPixmap(":/images/platforms/p1.png");
        x = 570/3*2;
        y = 0;
        w = 570/3;
        h = 250;
        break;
    //deblji tip platforme
    //------------
    //---788889---
    //---4....6---
    //---122223---
    //------------
    case '7' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 0;
        y = 0;
        w = 600/3;
        h = 600/3;
        break;
    case '8' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 600/3;
        y = 0;
        w = 600/3;
        h = 600/3;
        break;
    case '9' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 600/3*2;
        y = 0;
        w = 600/3;
        h = 600/3;
        break;
    case '4' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 0;
        y = 600/3;
        w = 600/3;
        h = 600/3;
        break;
    case '5' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 600/3;
        y = 600/3;
        w = 600/3;
        h = 600/3;
        break;
    case '6' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 600/3*2;
        y = 600/3;
        w = 600/3;
        h = 600/3;
        break;
    case '1' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 0;
        y = 600/3*2;
        w = 600/3;
        h = 600/3;
        break;
    case '2' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 600/3;
        y = 600/3*2;
        w = 600/3;
        h = 600/3;
        break;
    case '3' :
        image_type = QPixmap(":/images/platforms/tip2.png");
        x = 600/3*2;
        y = 600/3*2;
        w = 600/3;
        h = 600/3;
        break;
    // Donji coskovi
    case 'v' :
        image_type = QPixmap(":/images/platforms/left_corner.png");
        x = 10;
        y = 10;
        w = 150;
        h = 150;
        break;
    case 'b' :
        image_type = QPixmap(":/images/platforms/right_corner.png");
        x = 10;
        y = 10;
        w = 150;
        h = 150;
        break;
    // Jedinicna platforma
    case 's' :
        image_type = QPixmap(":/images/platforms/single.png");
        x = 0;
        y = 0;
        w = 420;
        h = 420;
        break;
    case 'w' :
        image_type = QPixmap(":/images/platforms/wall1.png");
        x = 0;
        y = 0;
        w = 80;
        h = 210;
        break;
    case 'W' :
        image_type = QPixmap(":/images/platforms/wall1.png");
        x = 0;
        y = 0;
        w = 80;
        h = 210;
        break;
    case 'G' :
        image_type = QPixmap(":/images/platforms/wall2.png");
        x = 0;
        y = 0;
        w = 80;
        h = 210;
        break;
    default:
        break;
    }
}
void Platform::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    painter->drawPixmap(this->rect(),  image_type, QRect(x, y, w, h));
}

