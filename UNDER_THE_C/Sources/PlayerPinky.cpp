#include "Headers/PlayerPinky.h"
#include <QGraphicsScene>

PlayerPinky::PlayerPinky(QGraphicsItem *parent): PlayerPlatform(parent){

 setPixmap(QPixmap(":/images/players/pinky/pinky_right_1.png"));
 setScale(0.1);

 img_side = 0;
 num_of_keys = 0;
 num_of_stars = 0;
 invincible=0;

}

void PlayerPinky::image_change()
{
    if(invincible != 0){
        invincible++;
        if(invincible >= 30)
            invincible =0;
    }
    // smena slika igraca prilikom kretanja na desnu stranu
    if(left_right_jump[1] == 1){

        if(img_change <= 3)
            setPixmap(QPixmap(":/images/players/pinky/pinky_right_2.png"));
        else if(img_change > 3 && img_change <= 6)
            setPixmap(QPixmap(":/images/players/pinky/pinky_right_3.png"));
        else if(img_change > 6 && img_change <= 9)
            setPixmap(QPixmap(":/images/players/pinky/pinky_right_4.png"));
        else if(img_change > 9){

            setPixmap(QPixmap(":/images/players/pinky/pinky_right_1.png"));

            if(img_change == 13)
                img_change = 0;
        }

        img_change++;
    }

    // smena slika igraca prilikom kretanja na levu stranu
    if(left_right_jump[0] == 1){

        if(img_change <= 4)
            setPixmap(QPixmap(":/images/players/pinky/pinky_left_2.png"));
        else if(img_change > 4 && img_change <= 9)
            setPixmap(QPixmap(":/images/players/pinky/pinky_left_3.png"));
        else if(img_change > 9 && img_change <= 12)
            setPixmap(QPixmap(":/images/players/pinky/pinky_left_4.png"));
        else if(img_change > 12){
            setPixmap(QPixmap(":/images/players/pinky/pinky_left_1.png"));

            if(img_change == 15)
                img_change = 0;
        }

        img_change++;
    }

    // ispisivanje slike igraca dok miruje
    if(left_right_jump[0] == 0 && left_right_jump[1] == 0 && img_change > 0){

        // promenljiva img_side odredjuje da li je poslednje pusten taster za levo ili za desno
        if(img_side == 1)
            setPixmap(QPixmap(":/images/players/pinky/pinky_left_1.png"));
        else
            setPixmap(QPixmap(":/images/players/pinky/pinky_right_1.png"));
        img_change = 0;
    }
}

void PlayerPinky::CollisionDoor()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    //standing = false;

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (Platform)){

            auto door = (qgraphicsitem_cast<Platform *>(colliding_items[i]));
            if(pos().x() + 48 <= door->pos().x()){//50
                if(left_right_jump[1] == 1)
                    setPos(door->pos().x()-15-60, y());
            }
        }
    }
}

/*int PlayerPinky::get_number_of_lianas(){
    return number_of_lianas;
}

void PlayerPinky:: in_number_of_lianas(){
    number_of_lianas++;
}*/
/***************
QPainterPath PlayerPinky::shape() const
{
    QPainterPath path;
    path.moveTo(0,0);
    path.addRect(200, 0, 400, 1400);
    return path;
}*/

