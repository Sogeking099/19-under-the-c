#include <QGraphicsScene>
#include "Headers/Key.h"
#include "QPainter"
#include <stdlib.h>
#include <QTimer>
#include <QDebug>
#include <Headers/PlayerMimi.h>

int Key::brojac=0;
Key::Key(QGraphicsItem *parent): StraightMotion(parent)
{
    setType(KEY);
    setIsAlive(true);
    int random=rand()%700;
    setPos(random, 700);
    setPixmap(QPixmap(":/images/collectables/key-clipart-11.png"));
    setScale(0.3);
    score_change = 0;
    ID=brojac;
    brojac++;
}

void Key::collision()
{
    if(getIsAlive()==false)
        return;

    auto colliding_items = collidingItems();


     foreach (auto item, colliding_items){
         if (typeid(*item) == typeid(PlayerMimi)){

             setIsAlive(false);
             score_change++;
             return;

         }
     }
}





