#include "Headers/Score.h"
#include <QFont>
#include<QFontDatabase>

Score::Score(QGraphicsItem * parent): QGraphicsTextItem(parent){
    score = 0;
    type=0;

    setPlainText(QString("Score: ") + QString::number(score));
    setDefaultTextColor(Qt::black);
    setFont(QFont("times", 16));
}
/*
Score::Score(int t)
{
    type = t;
    score = 0;

    if(type == 1){
        setPlainText(QString("Cristals: ") + QString::number(score) + QString("/5"));
        setDefaultTextColor(Qt::magenta);
        setFont(QFont("times",16));
    }
}*/

void Score::move(int player_x, int player_y)
{
     setPos(player_x - 100, player_y - 100);
}

Score::Score(int t){
    type = t;
    score = 0;

    if(type == 1){
        setPlainText(QString("Purple Cristals: ") + QString::number(score) + QString("/5"));
        setDefaultTextColor(Qt::magenta);
        setFont(QFont("times", 16));
    }
    else if(type == 2){
        setPlainText(QString("Stars: ") + QString::number(score) + QString("/4"));
        setDefaultTextColor(Qt::yellow);
        setFont(QFont("times", 16));
    }else if(type == 3){
        setPlainText(QString("Keys: ") + QString::number(score));
        setDefaultTextColor(Qt::magenta);
        setFont(QFont("times", 16));
    }
    else if(type == 4){
        setPlainText(QString("Golden cristal: ") + QString::number(score) + QString("/1"));
        setDefaultTextColor(Qt::yellow);
        setFont(QFont("times", 16));
    }
}
/*
void Score::move(int player_x, int player_y){
    setPos(player_x-200, player_y-100);
}*/

void Score::increase(){
    score++;
    if(type == 0){
        setPlainText(QString("Score: ") + QString::number(score));
    }else if(type == 1){
        setPlainText(QString("Purple Cristals: ") + QString::number(score) + QString("/5"));
    }else if(type == 2){
        setPlainText(QString("Stars: ") + QString::number(score) + QString("/4"));
    }else if(type == 3){
        setPlainText(QString("Keys: ") + QString::number(score));
    }else if(type == 4){
        setPlainText(QString("Golden cristal: ") + QString::number(score) + QString("/1"));
    }
}

void Score::decrease(){
    score--;
    setPlainText(QString("Keys: ") + QString::number(score));
}

int Score::decreaseByPoints(int number)
{
    if(score>=number){
        score=score-number;
        setPlainText(QString("Score: ") + QString::number(score));
        return score;
    }
       return -1;

}

void Score::increaseByPoints(int number)
{
    score=score+number;
    setPlainText(QString("Score: ") + QString::number(score));

}

int Score::getScore(){
    return score;
}
