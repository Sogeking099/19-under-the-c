#include "Headers/Stars.h"
#include <QTimer>
#include <Headers/Game.h>
#include <QGraphicsScene>
#include <Headers/Enemy.h>
//int Stars::brojac=0;
Stars::Stars(){

    setType(STARS);
    setIsAlive(true);
    setPixmap(QPixmap(":/images/attacks/gold_stars.png"));
    setScale(0.2);

}
