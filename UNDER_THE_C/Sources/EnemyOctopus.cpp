#include <Headers/EnemyOctopus.h>
#include <QGraphicsScene>
#include <Headers/Game.h>


EnemyOctopus::EnemyOctopus(QGraphicsPixmapItem *parent)
{

    setPixmap(QPixmap("://images/enemies/octopus/octopusGreen.png"));
    setPos(1300, 100);
    setScale(0.8);


    health_change=0;
    score_change=0;
    setIsAlive(true);
    setType(ENEMYOCTOPUS);
    healthBar= new HealthBar(x()+200, y()-5,300,20);
    healthBar->bar->setBrush(Qt::yellow);
    lives=5;
    img_change=0;
    timeout=0;



}

void EnemyOctopus::move()
{
    if(x()>600){
        setPos(x()-10,100);
    healthBar->bar->setPos(healthBar->bar->x()-10, healthBar->bar->y());
    healthBar->barFrame->setPos(healthBar->barFrame->x()-10, healthBar->barFrame->y());
    }
    else{

        if(getIsAlive()==false)
        return;
        collision();
    }
}

void EnemyOctopus::collision()
{
    auto colliding_items= collidingItems();
        foreach(auto item, colliding_items){
            if(typeid (*item)== typeid(PlayerMermaid) ){
               timeout=10;
               qgraphicsitem_cast<PlayerMermaid *>(item)->setPos(100,360);

             if(qgraphicsitem_cast<PlayerMermaid *>(item)->getShiled()==false){
                setHealthChange(1);

            }
             else{
                qgraphicsitem_cast<PlayerMermaid *>(item)->setShiled(false);
                setHealthChange(0);
             }
            }
            else if( typeid (*item)== typeid(Bubble) && getIsAlive() == true){

                 timeout=60;
                 setScoreChange(1);
                 qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                 setScoreChange(5);


                if(this->getNumberOfLives()==5){


                    scene()->removeItem(this->healthBar->bar);
                    delete(this->healthBar->bar);
                    this->healthBar->bar =new QGraphicsRectItem (x()+200, y()-5, 250,20);
                    this->healthBar->bar->setBrush(Qt::darkYellow);
                    qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                    scene()->addItem(this->healthBar->bar);
                    this->decreaseNumberofLives();
                    return;
                }
               else if(this->getNumberOfLives()==4){


                    scene()->removeItem(this->healthBar->bar);
                    delete(this->healthBar->bar);
                    this->healthBar->bar =new QGraphicsRectItem (x()+200, y()-5, 200,20);
                    this->healthBar->bar->setBrush(Qt::darkMagenta);
                    qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                    scene()->addItem(this->healthBar->bar);
                    this->decreaseNumberofLives();
                    return;
                }
                else if(this->getNumberOfLives()==3){



                     scene()->removeItem(this->healthBar->bar);
                     delete(this->healthBar->bar);
                     this->healthBar->bar =new QGraphicsRectItem (x()+200, y()-5, 150,20);
                     this->healthBar->bar->setBrush(Qt::darkRed);
                     qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                     scene()->addItem(this->healthBar->bar);
                     this->decreaseNumberofLives();
                     return;
                 }
                else if(this->getNumberOfLives()==2){



                     scene()->removeItem(this->healthBar->bar);
                     delete(this->healthBar->bar);
                     this->healthBar->bar =new QGraphicsRectItem (x()+200, y()-5, 100,20);
                     this->healthBar->bar->setBrush(Qt::red);
                     qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                     scene()->addItem(this->healthBar->bar);
                     this->decreaseNumberofLives();
                     return;
                 }

                else{

                    setIsAlive(false);
                    scene()->removeItem(this->healthBar->bar);
                    scene()->removeItem(this->healthBar->barFrame);

                    qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                    this->decreaseNumberofLives();
                    delete(this->healthBar->barFrame);
                    delete(this->healthBar->bar);
                    img_change=0;
                    return;

                }
            }

        }

}

void EnemyOctopus::image_change()
{


    if(timeout==0){
        setPixmap(QPixmap(":/images/enemies/octopus/octopusGreen.png"));
        setScale(0.8);

   }


    else{

    if(img_change >=0 && img_change < 2){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusGreen1.png"));
         setScale(0.8);

        img_change++;
    }
    else if(img_change >=2 && img_change < 4){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusGreen2.png"));
         setScale(0.8);


        img_change++;
    }
    else if(img_change >= 4 && img_change <6){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusGreen3.png"));
         setScale(0.8);

        img_change++;
    }
    else if(img_change ==6){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusGreen2.png"));
         setScale(0.8);
            img_change = 0;
    }
    }
}

void EnemyOctopus::end_image_change()
{
    if(img_change >=0 && img_change < 20){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusDelete.png"));
         setScale(0.8);

        img_change++;
    }
    else if(img_change >=20&& img_change <40){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusDelete1.png"));
         setScale(0.8);


        img_change++;
    }
    else if(img_change >=40 && img_change <55){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusDelete2.png"));
         setScale(0.8);

        img_change++;
    }
    else if(img_change >=55 && img_change <60){

         setPixmap(QPixmap(":/images/enemies/octopus/octopusDelete3.png"));
         setScale(0.8);
         img_change++;
    }
    else{
        setPixmap(QPixmap(":/images/enemies/octopus/octopusGold.png"));
        setScale(0.8);
    }
}


int EnemyOctopus::getNumberOfLives()
{
    return lives;
}

void EnemyOctopus::decreaseNumberofLives()
{
    lives--;
}

int EnemyOctopus::getTimeout()
{
    return timeout;
}

void EnemyOctopus::decraseTimeout()
{
    timeout--;
}
