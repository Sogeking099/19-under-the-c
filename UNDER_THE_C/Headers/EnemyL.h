#ifndef ENEMYL_H
#define ENEMYL_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QPainter>
#include<QGraphicsPixmapItem>
#include "ItemOfScene.h"
#include "Score.h"
#include <Headers/StraightMotion.h>
#include <Headers/EnemiesWithScore.h>

class EnemyL: public StraightMotion, public EnemiesWithScore{

    Q_OBJECT
public:

    EnemyL(int numberOfLife=1,StraightMotion * parent=0);

    int ID;
    static int brojac;

    int getNumberOfLife() const;
    void incNumberOfLife();

    int getDX();
    void setDX(int value);
    int getDX1();
    void setDX1(int value);
    void moveP();


private:
    bool direction;
    int sizeOfPath;
    int dx;
    int dx1;
};

#endif // ENEMYL_H
