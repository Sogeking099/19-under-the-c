#ifndef HEALTH_H
#define HEALTH_H

#include <QGraphicsTextItem>
#include <QFont>
#include <QObject>

#include <QLabel>
class Health: public QGraphicsTextItem{
   // Q_OBJECT
public:
    Health(QGraphicsItem * parent= 0);
    void move(int player_x, int player_y);
    void decrease();
    int getHealth();
    void setHealth(int value);
private:
    int health;
};


#endif // HEALTH_H
