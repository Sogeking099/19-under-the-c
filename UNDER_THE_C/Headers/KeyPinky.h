#ifndef KEYPINKY_H
#define KEYPINKY_H

#include <QGraphicsRectItem>
#include <QObject>
#include <Headers/StraightMotion.h>
#include <Headers/Collectable.h>

class KeyPinky:public StraightMotion, public Collectable{
    Q_OBJECT
public:
    KeyPinky(QGraphicsItem * parent=0);
    static int brojac;
    int ID;
    void collision();
};

#endif // KEYPINKY_H
