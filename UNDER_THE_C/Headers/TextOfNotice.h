#ifndef TEXTOFNOTICE_H
#define TEXTOFNOTICE_H

#include <QGraphicsTextItem>


class TextOfNotice: public QGraphicsTextItem{
public:
    TextOfNotice();
    void setText(QString text);
    void setDuration(int sek);
    int getDuration();
    bool IsOnScene();
    void ItemOnScene(bool value);
private:
    int duration;
    bool onScene;


};

#endif // TEXTOFNOTICE_H
