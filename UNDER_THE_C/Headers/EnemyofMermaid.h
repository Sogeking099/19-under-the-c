#ifndef ENEMYOFMERMAID_H
#define ENEMYOFMERMAID_H
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QPainter>
#include <Headers/ItemOfScene.h>
#include <Headers/HealthBar.h>
#include <Headers/EnemiesWithScore.h>
class EnemyofMermaid: public ItemOfScene, public EnemiesWithScore{
    Q_OBJECT
public:

    EnemyofMermaid(QGraphicsPixmapItem *parent=nullptr);
    static double speedUp;
    //static int counter;
    //int ID;
    void move();
    void shoot();
    void collision();
    int width();
    HealthBar *healthBar;
    int lives;
    int getNumberOfLives();
    void decreaseNumberofLives();


};
#endif // ENEMYOFMERMAID_H
