#ifndef MINIWINGS_H
#define MINIWINGS_H
#include "ItemOfScene.h"
#include "PlayerMimi.h"
#include <QGraphicsScene>
#include <QDebug>
#include <Headers/StraightMotion.h>

class WingsMini: public StraightMotion{
     Q_OBJECT

public:
    WingsMini(QGraphicsItem *parent=nullptr);
    void collision();
    void setWingsChange(bool value);
    bool getWingsChange();
private:
    bool wings_change;

};

#endif // MINIWINGS_H
