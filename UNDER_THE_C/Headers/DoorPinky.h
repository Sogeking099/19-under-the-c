#ifndef DOORPINKY_H
#define DOORPINKY_H

#include <Headers/Platform.h>
#include <QGraphicsPixmapItem>
#include <Headers/PlayerPinky.h>
#include <Headers/StraightMotion.h>
#include <Headers/Collectable.h>

class DoorPinky:public ItemOfScene, public Collectable{
    Q_OBJECT
public:
    DoorPinky(QGraphicsItem * parent=0);
    void collision(DoorPinky* id);
    static int brojac;
    int ID;
    int getX();
    void setX(int i);
private:
    int x;
};

#endif // DOORPINKY_H
