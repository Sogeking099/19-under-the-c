#ifndef PLAYERMERMAID_H
#define PLAYERMERMAID_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>

class PlayerMermaid: public QObject, public QGraphicsPixmapItem{
        Q_OBJECT
public:
    PlayerMermaid(QGraphicsItem *parent = nullptr);
   // void keyPressEvent(QKeyEvent * event);
    QGraphicsPixmapItem *bubble;
    int get_number_of_bubbles();
    void in_number_of_bubbles();
    void image_change();
    bool shiled;
    bool getShiled();
    void setShiled(bool state);
    void moveLeft();
    void moveRight();
    void moveDown();
    void moveUp();
private:
    int number_of_bubbles;
    int img_change;

};

#endif // PLAYERMERMAID_H
