#ifndef CLOUD_H
#define CLOUD_H
#include <QGraphicsScene>
#include "ItemOfScene.h"
#include <Headers/StraightMotion.h>
class Cloud:public StraightMotion{
public:
    Cloud(QGraphicsItem * parent=0);
    static double ubrzanjeCloud;
};

#endif // CLOUD_H
