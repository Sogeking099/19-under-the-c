#ifndef LEVELPLATFORM_H
#define LEVELPLATFORM_H

#include <Headers/Levelscene.h>

#include <QMap>
#include <QVector>
#include <QString>
#include <QTextStream>
#include <QPainter>
#include <QKeyEvent>
#include <iostream>

#include <Headers/Platform.h>
#include <Headers/PlayerPlatform.h>
#include <Headers/Sword.h>
#include <Headers/ItemOfScene.h>
#include <Headers/EnemySpider.h>
#include <Headers/EnemyThorn.h>
#include <Headers/EnemyDeathRay.h>
#include <Headers/EnemyFly.h>
#include <Headers/SpiderBoss.h>
#include <Headers/StaticSceneObjects.h>
#include <Headers/SceneDecoration.h>
#include <Headers/Cristal.h>
#include <Headers/Door.h>

class LevelPlatform: public LevelScene
{
    Q_OBJECT
public:
    LevelPlatform(QObject * parent = nullptr);
    void DeleteLevelPlatform();
    void initialize();
public slots:
    void onSetMute();
    void update();
    void onClicked();
/*signals:
    void positionHealth(Health* h);*/
private:
    void keyPressEvent(QKeyEvent * event);
    void keyReleaseEvent(QKeyEvent* event);
    void bossAttack2();
    QVector<StaticSceneObjects*> objects;
    QMap<int, ItemOfScene*> items;
    PlayerPlatform * player;
    Sword* sword;
    Health* h;
    Score* s;
    bool pause;
    bool stopEventKey;
    unsigned int soundTimer;
    unsigned int soundTimer2;
    bool battle_activated;
    Button *buttonRules;
    bool onRules;
    Button *musicButton;
};

#endif // LEVELPLATFORM_H
