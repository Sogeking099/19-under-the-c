#ifndef TREASURE_H
#define TREASURE_H

#include <QGraphicsRectItem>
#include <QObject>
#include <Headers/ItemOfScene.h>

class Treasure:public ItemOfScene{
public:
    Treasure(QGraphicsItem * parent=0);
    static int brojac;
    int ID;
};

#endif // TREASURE_H
