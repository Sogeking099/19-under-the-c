#ifndef BUTTON_H
#define BUTTON_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QString>
class Button:public QObject, public QGraphicsPixmapItem{
  Q_OBJECT
public:
    Button(QGraphicsItem *parent=0);
    Button(int number);
    void mousePressEvent(QGraphicsSceneMouseEvent *event );
    void initialize(QString name, double scale, double width, double height);


signals:
    void clicked();
     void clicked_level(int chosen_level);

private:
    int chosen_level=-1;
};



#endif // BUTTON_H
