#ifndef INK_H
#define INK_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <Headers/ItemOfScene.h>
#include <Headers/StraightMotion.h>
#include <Headers/Enemies.h>
#include <Headers/Bubble.h>

class Ink:public StraightMotion, public Enemies{
    Q_OBJECT
public:
    Ink();
    void collision();



};
#endif // INK_H
