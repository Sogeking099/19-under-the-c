#ifndef SPIDERBOSS_H
#define SPIDERBOSS_H

#include <Headers/ItemOfScene.h>
#include <Headers/Sword.h>
#include <QGraphicsScene>
#include <Headers/PlayerPlatform.h>
#include <Headers/Enemies.h>
#include <Headers/ItemWithSounds.h>

class SpiderBoss: public ItemOfScene, public Enemies, public ItemWithSounds
{
    Q_OBJECT
public:
    SpiderBoss();
    void findPlayer();
    void move(int player_x);
    void collisions();
    void image_change();
    bool getBattle();
    bool getVulnerable();
    int getTimeout();
    void decraseTimeout();
    int getDefense();
    void stopDefense();
    void startDefense();
    void increaseDefense();
    int attack();
private:
    bool battle;
    int img_change;
    int attack1;
    int attack2;
    int rest;
    int rest_count;
    bool vulnerable;
    int defense;
    int timeout;

};

#endif // SPIDERBOSS_H
