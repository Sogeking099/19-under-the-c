#ifndef ENEMYOCTOPUS_H
#define ENEMYOCTOPUS_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QPainter>
#include <Headers/ItemOfScene.h>
#include <Headers/HealthBar.h>
#include <Headers/EnemiesWithScore.h>
#include <Headers/PlayerMermaid.h>
#include <Headers/Bubble.h>
class EnemyOctopus: public ItemOfScene, public EnemiesWithScore{
    Q_OBJECT
public:

    EnemyOctopus(QGraphicsPixmapItem *parent=nullptr);
    static double speedUp;

    void move();
    void collision();
    void image_change();
    void end_image_change();
    HealthBar *healthBar;
    int lives;
    int getNumberOfLives();
    void decreaseNumberofLives();
    int getTimeout();
    void decraseTimeout();


private:
    int img_change;
    int timeout;




};

#endif // ENEMYOCTOPUS_H
