#ifndef STRAIGHTMOTION_H
#define STRAIGHTMOTION_H

#include <QGraphicsItem>
#include <Headers/ItemOfScene.h>

class StraightMotion: public ItemOfScene
{
public:
    StraightMotion(QGraphicsItem *parent=0);
    void move(int speed_item);
    void move(int side, int speed, int scene_border);
private:
    int height();
    int width();
};

#endif // STRAIGHTMOTION_H
