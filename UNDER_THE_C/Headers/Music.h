#ifndef MUSIC_H
#define MUSIC_H

#include <QMediaPlayer>
#include <QVector>


class Music{


public:
    void pushBackSound(const QString &url);
    void checkMute();
    void setMute();
    void makeSound(int indexOfElemt);
    void deleteSounds();
    void pauseSonuds();
    void stopSound(int indexOfElemt);
    void playSounds();
    void playSound(int indexOfElemt);
    int size();
static bool mute;


private:
    QVector<QMediaPlayer*>musics;


};

#endif // MUSIC_H
