#ifndef RULES_H
#define RULES_H

#include <QGraphicsPixmapItem>

class Rules: public QGraphicsPixmapItem
{
public:
    Rules();
    void setNumberOfLevel(int numberOfLevel);
};

#endif // RULES_H
