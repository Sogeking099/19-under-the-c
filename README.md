# Project 19-Under-the-C
🍓 Demo: https://drive.google.com/file/d/1qKwlbRyV70uofJHHg3TX2vzwWxK67p-0/view?usp=drive_web 

🍎 Walkthrough second level: https://drive.google.com/file/d/11X2XCKbZpx2YryrMEro0-V2LMMRL3eYx/view

🍊 Walkthrough fourth level: https://drive.google.com/file/d/1-8o5pJGbtWmgJgSn21CXmClN275ntynY/view?usp=sharing

🍒 Tutorial: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/19-under-the-c/-/wikis/Tutorijal

🍉 Starting game: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/19-under-the-c/-/wikis/Pokretanje

Under the C is 2D game that follows four girls in their adventure of finding secret treasure hidden in castle under the sea. Game has total of four levels. Each level has different character for player, different enemies and gameplay logic. We believe that by using different logics in levels we are tickling the curiosity of players making them want to know what is new that next level will bring. Also it will be a challange for player to practice new movements and adapt to new surounding all while playing one game. Goal of first three levels is to free character that will be used for next level while goal for last level is to find the treasure.

## Developers

- [Angelina Jordanov, 422/2019](https://gitlab.com/Sogeking099)
- [Katarina Popovic, 361/2020](https://gitlab.com/katarina.popovic)
- [Irina Marko, 243/2017](https://gitlab.com/irinamarko)
- [Milana Novakovic, 467/2018](https://gitlab.com/milana98)
